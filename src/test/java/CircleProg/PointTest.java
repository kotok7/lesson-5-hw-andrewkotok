package CircleProg;


import org.junit.Assert;
import org.junit.jupiter.api.Test;

class PointTest {


    @Test
    void shouldCalculateDistanceTo() {
        Circle circle = new Circle(2.0, 2.0);
        Point point = new Point(4.0, 4.0);
        Assert.assertEquals(2.83, point.distanceTo(circle));
    }
}