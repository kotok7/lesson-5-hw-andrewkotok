package CircleProg;

import java.util.Scanner;

public class Circle {
    final double xCircle;
    final double yCircle;

    public Circle(double xCircle, double yCircle) {
        this.xCircle = xCircle;
        this.yCircle = yCircle;
    }

    public static void circleIn() {
        System.out.println("Please, enter circle information (coordinate X, Coordinate Y, radius): ");
    }

    @Override
    public String toString() {
        return "Circle{" +
                "xCircle=" + xCircle +
                ", yCircle=" + yCircle +
                '}';
    }


}
