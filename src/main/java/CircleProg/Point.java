package CircleProg;


import java.util.Scanner;

public class Point {
    Scanner sc = new Scanner(System.in);
    private final double x;
    private final double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }


    public double distanceTo(Circle p) {
        double dx = x - p.xCircle;
        double dy = y - p.yCircle;
        return Math.sqrt(dx * dx + dy * dy);
    }


    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

}
