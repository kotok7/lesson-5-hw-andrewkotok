package CircleProg;


import java.util.Arrays;
import java.util.Scanner;

import static CircleProg.Circle.circleIn;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        circleIn();
        Circle circle = new Circle(getOperand(sc), getOperand(sc));
        double radius = getOperand(sc);
        sc.nextLine();
        System.out.println(circle);

        Point[] points = new Point[2];
        for (int i = 0; i < points.length; i++) {
            System.out.println("Please enter coordinates X and Y");
            points[i] = new Point(getOperand(sc), getOperand(sc));
        }
        System.out.println(Arrays.toString(points));

        for (int i = 0; i < points.length; i++) {

            if (points[i].distanceTo(circle) < radius) {
                System.out.println("Point number " + (i + 1) + " is in a circle");
            } else if (points[i].distanceTo(circle) > radius) {
                System.out.println("Point number " + (i + 1) + " is out of a circle");
            } else {
                System.out.println("Point is on the circle line");
            }
        }
    }


    private static double getOperand(Scanner sc) {
        return sc.nextDouble();
    }
}
